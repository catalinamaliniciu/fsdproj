import { NextFunction, Request, Response } from "express";
import * as tf from '@tensorflow/tfjs-node';

import MnistData from '../../data';

export class ImageController {

  async evaluate(request: Request, response: Response, next: NextFunction) {
    const model = await tf.loadLayersModel('file://./model.json');
    const data = new MnistData();
    await data.load();
    return await this.doPrediction(model, data, 1);
  }


  async doPrediction(model, data, testDataSize = 500) {
    const IMAGE_WIDTH = 28;
    const IMAGE_HEIGHT = 28;
    const testData = data.nextTestBatch(testDataSize);
    const testxs = testData.xs.reshape([testDataSize, IMAGE_WIDTH, IMAGE_HEIGHT, 1]);
    const labels = testData.labels.argMax(-1);
    const preds = model.predict(testxs).argMax(-1);
    testxs.dispose();
    preds.print()
    return [preds, labels];
  }

  async train(request: Request, response: Response, next: NextFunction) {
    const model = await tf.loadLayersModel('file://./model.json');
    const data = new MnistData();
    await data.load();
    model.compile({
      optimizer: tf.train.adam(),
      loss: 'categoricalCrossentropy',
      metrics: ['accuracy']
    });
    const trainResult = await this.trainModel(model, data);
    await model.save('file://./after-train');
    return trainResult;
  }


  async upload(request: Request, response: Response, next: NextFunction) {
    const model = await tf.loadLayersModel('file://./after-train/model.json');
    let tensor = tf.node.decodePng(request.files.image.data, 1)
    tensor = tensor.reshape([-1, 28, 28, 1])
    let preds = model.predict(tensor) as tf.Tensor;
    preds = preds.argMax(-1);
    let tensorData = preds.dataSync();
    return tensorData[0].toString();
  }

  toArrayBuffer(buf) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
      view[i] = buf[i];
    }
    return ab;
  }

  async trainModel(model, data) {
    const BATCH_SIZE = 256;
    const TRAIN_DATA_SIZE = 5500;
    const TEST_DATA_SIZE = 1000;

    const [trainXs, trainYs] = tf.tidy(() => {
      const d = data.nextTrainBatch(TRAIN_DATA_SIZE);
      return [
        d.xs.reshape([TRAIN_DATA_SIZE, 28, 28, 1]),
        d.labels
      ];
    });

    const [testXs, testYs] = tf.tidy(() => {
      const d = data.nextTestBatch(TEST_DATA_SIZE);
      return [
        d.xs.reshape([TEST_DATA_SIZE, 28, 28, 1]),
        d.labels
      ];
    });

    return model.fit(trainXs, trainYs, {
      batchSize: BATCH_SIZE,
      validationData: [testXs, testYs],
      epochs: 30,
      shuffle: true,
    });
  }



}