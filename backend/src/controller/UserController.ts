import { getRepository } from "typeorm";
import { Request, Response } from "express";
import { User } from "../entity/User";
import * as jwt from 'jsonwebtoken';
import { JwtPayload } from '../interfaces/jwt-payload';
import { UserRO } from '../entity/User.ro';

export class UserController {

  private userRepository = getRepository(User);

  async all() {
    return this.userRepository.find();
  }

  async one(request: Request) {
    return this.userRepository.findOne(request.params.id);
  }

  async save(request: Request) {
    return this.userRepository.save(request.body);
  }

  async remove(request: Request) {
    let userToRemove = await this.userRepository.findOne(request.params.id);
    await this.userRepository.remove(userToRemove);
  }

  public async login(request: Request, response: Response) {
    const email = request.body.email;
    const password = request.body.password;
    // let user = await this.userRepository.findOne({ where: { email } });
    // if (!user || user.password != request.body.password) {
    //   return "User doesn't exists!";
    // }

    // return this.createToken(user);
    const user = await this.userRepository.findOne({ where: { email } });
    if (user && user.comparePassword(password)) {
      return this.createToken(user);
    }
    return "User doesn't exists!";

  }
  public async register(request: Request, response: Response) {
    const email = request.body.email;
    let user = await this.userRepository.findOne({ where: { email } });
    if (!user) {
      return "User already exists!";
    }
    user = request.body;
    user = await this.userRepository.save(user);
    return user;
  }


  createToken(user: User) {
    const expiresIn = 3600;

    const accessToken = jwt.sign(
      {
        id: user.id,
        email: user.email,
        firstname: user.firstName,
        lastname: user.lastName,
        age: user.age
      },
      'jwtAuthentificationFSD',
      { expiresIn }, 
    );

    return {
      expiresIn,
      accessToken,
    };
  }

  async validateUserToken(payload: JwtPayload): Promise<User> {
    return await this.one(payload.id);
  }

  async validateUser(email: string, password: string): Promise<UserRO> {
    const user = await this.userRepository.findOne({ where: { email } });
    if (user && user.comparePassword(password)) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }


}