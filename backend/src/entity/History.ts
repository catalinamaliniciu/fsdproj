import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class History{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    size: number;

    @Column()
    recognitionResult: string;

    @Column()
    link: string;
}