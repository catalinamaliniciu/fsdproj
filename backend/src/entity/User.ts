import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";
import {UserRO} from "./User.ro"
import * as bcrypt from 'bcrypt';
import { BeforeInsert } from "typeorm";


@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    age: number;

    @Column()
    email: string;

    @Column()
    password: string;

    @BeforeInsert()
   async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }
  
  async comparePassword(attempt: string): Promise<boolean> {
    return await bcrypt.compare(attempt, this.password);
  }

   toResponseObject(showToken: boolean = true): UserRO {
    const { id, firstName, lastName, email, age } = this;
    const responseObject: UserRO = {
      id,
      firstName,
      lastName,
      email,
      age
    };

    return responseObject;
  }



}
