import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import {User} from "./entity/User";
import { History } from "./entity/History";
import { checkJwt } from "./controller/TokenValidation";
const fileUpload = require('express-fileupload')

createConnection().then(async connection => {

    // create express app
    const app = express();
    var cors = require('cors')
    app.use(bodyParser.json());
    app.use(fileUpload({
        useTempFiles: false,
        tempFileDir: '/tmp/'
    }));
    app.use(cors())

    // register express routes from defined application routes
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            if(route.guards != null && route.guards != {}) {
                checkJwt(req.headers.authorization, res);
            }
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
    // await connection.manager.save(connection.manager.create(User, {
    //     email: "e1@email.com",
    //     password:"Pass1",
    //     firstName: "Timber",
    //     lastName: "Saw",
    //     age: 27
    // }));
    // await connection.manager.save(connection.manager.create(User, {
    //     email: "e2@email.com",
    //     password:"Pass2",
    //     firstName: "Phantom",
    //     lastName: "Assassin",
    //     age: 24
    // }));
    await connection.manager.save(connection.manager.create(User, {
        email: "a@email.com",
        password:"Pass12345",
        firstName: "Jackson",
        lastName: "Alex",
        age: 24
    }));

    // // insert history for test
    // await connection.manager.save(connection.manager.create(History, {
    //     name: "Image A",
    //     size: 20,
    //     recognitionResult: "recognition result image A",
    //     link:"linkToDownloadImgA"
    // }));

    // await connection.manager.save(connection.manager.create(History, {
    //     name: "Image B",
    //     size: 50,
    //     recognitionResult: "recognition result image B",
    //     link:"linkToDownloadImgB"
    // }));

    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => console.log(error));
