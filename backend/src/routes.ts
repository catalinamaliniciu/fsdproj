import { HistoryController } from "./controller/HistoryController";
import {UserController} from "./controller/UserController";
import {checkJwt} from "./controller/TokenValidation"
import { ImageController } from "./controller/ImageController";

export const Routes = [{
    method: "get",
    guards: checkJwt,
    route: "/history",
    controller: HistoryController,
    action: "all"
},{
    method: "get",
    route: "/users",
    //guards: checkJwt,
    controller: UserController,
    action: "all"
}, {
    method: "get",
    route: "/users/:id",
    guards: checkJwt,
    controller: UserController,
    action: "one"
}, {
    method: "post",
    route: "/users",
   // guards: checkJwt,
    controller: UserController,
    action: "save"
}, {
    method: "delete",
    route: "/users/:id",
    guards: checkJwt,
    controller: UserController,
    action: "remove"
}, {
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login"
}, {
    method: "post",
    route: "/register",
    controller: UserController,
    action: "register"
}, {
    method: "get",
    route: "/evaluate",
    controller: ImageController,
    action: "evaluate"
}, {
    method: "get",
    route: "/train",  
    controller: ImageController,
    action: "train"  // numele functiei
},
{
    method:"post",
    route:"/upload",
    controller: ImageController,
    action: "upload"

},
{
    method:"post",
    route:"/history",
    controller: HistoryController,
    action: "save"
},
{
    method: "delete",
    route: "/history/:id",
    controller: HistoryController,
    action: "remove"
}

];