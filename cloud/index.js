//Imports the Google Cloud client library
var Datastore = require('@google-cloud/datastore').Datastore;
// // Creates a client
var datastore = new Datastore({
    projectId: 'fsdproject',
    keyFilename: 'fsdproject-fe6db3f04668.json'
});
function helloWorld(req, res) {
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'GET, POST');
    var query = datastore.createQuery('user-log');
    var logs = datastore.runQuery(query).then(function (entity) {
        console.log(entity);
        res.status(200).send(entity);
    }).catch(function (err) {
        console.error('ERROR:', err);
        res.status(200).send(err);
        return;
    });
}
exports.helloWorld = helloWorld;
;
function getPosts(req, res) {
    //test
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', '*');
    var query = datastore.createQuery('post');
    var logs = datastore.runQuery(query).then(function (entity) {
        console.log(entity);
        res.status(200).send(entity);
    }).catch(function (err) {
        console.error('ERROR:', err);
        res.status(200).send(err);
        return;
    });
}
exports.getPosts = getPosts;
function createPost(req, res) {
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'POST');
    res.set('Access-Control-Allow-Headers', '*');
    var title = req.query.title || req.body.title || 'default title';
    var author = req.query.author || req.body.author || 'default author';
    var body = req.query.body || req.body.body || 'default body';
    var postedOn = req.query.postedOn || req.body.postedOn || new Date().toJSON();
    var taskKey = datastore.key('post');
    var entity = {
        key: taskKey,
        data: [
            {
                name: 'postedOn',
                value: postedOn
            },
            {
                name: 'title',
                value: title,
                excludeFromIndexes: true
            },
            {
                name: 'body',
                value: body,
                excludeFromIndexes: true
            },
            {
                name: 'author',
                value: author,
                excludeFromIndexes: true
            },
        ]
    };
    try {
        datastore.save(entity).then(function (entity) {
            console.log(entity);
            res.status(200).send(entity);
        }).catch(function (err) {
            console.error('ERROR:', err);
            res.status(200).send(err);
            return;
        });
        console.log("Task " + taskKey.id + " created successfully.");
    }
    catch (err) {
        console.error('ERROR:', err);
    }
}
exports.createPost = createPost;
