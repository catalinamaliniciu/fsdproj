//Imports the Google Cloud client library
const {Datastore} = require('@google-cloud/datastore');

const datastore = new Datastore({
  projectId: 'fsdproject',
  keyFilename: 'fsdproject-fe6db3f04668.json'
});

export function helloWorld(req, res) {
  res.set('Access-Control-Allow-Origin', "*")
  res.set('Access-Control-Allow-Methods', 'GET, POST')
  const query = datastore.createQuery('user-log');
  const logs = datastore.runQuery(query).then(entity => {
    console.log(entity);
    res.status(200).send(entity);
  }).catch(err => {
    console.error('ERROR:', err);
    res.status(200).send(err);
    return;
  });
  
};

export function getPosts(req, res){
  res.set('Access-Control-Allow-Origin', "*")
  res.set('Access-Control-Allow-Methods', 'GET')
  res.set('Access-Control-Allow-Headers', '*');
  const query = datastore.createQuery('post');
  const logs = datastore.runQuery(query).then(entity => {
    console.log(entity);
    res.status(200).send(entity);
  }).catch(err => {
    console.error('ERROR:', err);
    res.status(200).send(err);
    return;
  });
}

export function createPost(req, res){
  res.set('Access-Control-Allow-Origin', "*")
  res.set('Access-Control-Allow-Methods', 'POST')
  res.set('Access-Control-Allow-Headers', '*');
  let title = req.query.title || req.body.title || 'default title';
  let author = req.query.author || req.body.author || 'default author';
  let body = req.query.body || req.body.body || 'default body';
  let postedOn = req.query.postedOn || req.body.postedOn || new Date().toJSON();

  const taskKey = datastore.key('post');
  const entity = {
    key: taskKey,
    data: [
      {
        name: 'postedOn',
        value: postedOn,
      },
      {
        name: 'title',
        value: title,
        excludeFromIndexes: true,
      },
      {
        name: 'body',
        value: body,
        excludeFromIndexes: true,
      },
      {
        name: 'author',
        value: author,
        excludeFromIndexes: true,
      },
    ],
  };

  try {
    datastore.save(entity).then(entity => {
      console.log(entity);
      res.status(200).send(entity);
    }).catch(err => {
      console.error('ERROR:', err);
      res.status(200).send(err);
      return;
    });
    console.log(`Task ${taskKey.id} created successfully.`);
  } catch (err) {
    console.error('ERROR:', err);
  }
}
