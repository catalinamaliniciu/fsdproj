import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryComponent } from './history/history.component';
import { LoginComponent } from './login/login.component';
import { UploadComponent } from './upload/upload.component';
import { AuthGuard } from './guards/auth-guard.guard';
import { CloudComponent } from './cloud/cloud.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'cloud', component: CloudComponent },
  { path: 'login', component: LoginComponent },
  { path: 'history', component: HistoryComponent, canActivate: [AuthGuard]},
  { path: 'upload', component: UploadComponent, canActivate: [AuthGuard]},
  { path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
