import { Component } from '@angular/core';
import { HeaderStateService } from './service/header-state/header-state.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'laborator2';
  show = false;
  constructor(
    private _headerService: HeaderStateService
  ) {
    _headerService.emitter.subscribe(data => {
      this.show = data
    })
  }
}
