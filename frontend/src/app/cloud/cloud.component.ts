import { Component, Inject, OnInit } from '@angular/core';
import { CloudService } from '../service/cloud.service'
import { Post } from '../interfaces/Post'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-cloud',
  templateUrl: './cloud.component.html',
  styleUrls: ['./cloud.component.scss']
})
export class CloudComponent implements OnInit {

  data = {}
  postArray: Post[]
  postData: Post = {
    title: '',
    body: '',
    author: '',
    postedOn: new Date()
  }
  constructor(
    private _cloudService: CloudService,
    public dialog: MatDialog
  ) { }


  ngOnInit(): void {
    // this._cloudService.getCloud('https://us-central1-fsdproject.cloudfunctions.net/helloWorld').subscribe({
    //  next: (val: any) => { this.data = JSON.stringify(val[0]);},
    // error: (error: any) => console.error(error)
    // });

    this._cloudService.getCloud('https://us-central1-fsdproject.cloudfunctions.net/getPosts').subscribe({
      next: (val: any) => { this.postArray = val[0]; },
      error: (error: any) => console.error(error)

    });

  }


  openDialog(): void {
    const dialogRef = this.dialog.open(AddPostDialog, {
      width: '400px',
      data: {
        title: this.postData.title,
        body: this.postData.body,
        author: this.postData.author,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.postData = {
          ...result,
          postedOn: new Date().toJSON()
        };
        this.postArray.push(this.postData);
        this._cloudService.createPost('https://us-central1-fsdproject.cloudfunctions.net/createPost', this.postData).subscribe(
          (val) => console.log(val)
        )
      }
    });
  }
}


@Component({
  selector: 'add-post-dialog',
  templateUrl: 'add-post-dialog.html',
})
export class AddPostDialog {

  constructor(
    public dialogRef: MatDialogRef<AddPostDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Post) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}