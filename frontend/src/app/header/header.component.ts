import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderStateService } from '../service/header-state/header-state.service';
import { TokenService } from '../service/token.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router,
    private _headerService: HeaderStateService,
    private _tokenService: TokenService) { }

  ngOnInit(): void {
  }

  onClickHistory(){
    this.router.navigate(["history"])
  }

  onClickUpload(){
    this.router.navigate(["upload"])
  }

  onClickLogout(){
    this._tokenService.removeToken();
    this._headerService.hide();

  }

}
