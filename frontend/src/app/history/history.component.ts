import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import {HistoryService} from '../service/history.service'
import {History} from '../service/history.service'
import { tap } from 'rxjs/operators';
import { HeaderStateService } from '../service/header-state/header-state.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  historyData: History[]

  constructor(private historyService: HistoryService,
    private _headerService: HeaderStateService,
    ) {
    this._headerService.show()
  }

  ngOnInit(): void {
    this.historyService
    .getHistory('http://localhost:3000/history')
    .pipe(
      tap((data: History[]) => console.log(data[0]))
    ).subscribe((history: History[]) => {
      this.historyData = history; 
      this.dataSource = new MatTableDataSource<History>(this.historyData);
    }); 
    
  }

  displayedColumns: string[] = ['select', 'name', 'size', 'recognitionResult', 'link'];
  dataSource = new MatTableDataSource<History>();
  selection = new SelectionModel<History>(true, []);

   /** Whether the number of selected elements matches the total number of rows. */
   isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
}
