import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../service/user-service/user.service'
import { HeaderStateService } from '../service/header-state/header-state.service'
import { RestRequestService } from '../service/rest-request/rest-request.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent  {

  formLogin = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
  });
  
  constructor(
    private router: Router,
    private _userService: UserService,
    private _restRequestService: RestRequestService,
    private _headerService: HeaderStateService,

  ) {
    _headerService.hide();
  }

  login(): void {
    if(this.formLogin.valid) {
      this._userService.login(this.formLogin.value).subscribe({
        next: () => {this.router.navigate(['history']);},
        error: error => console.error(error)   
      })
     
    }
  }

}
