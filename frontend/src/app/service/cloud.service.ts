import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {Post} from '../interfaces/Post'

import { RestRequestService } from './rest-request/rest-request.service';
@Injectable({
  providedIn: 'root'
})
export class CloudService {

  constructor(
    private _restService: RestRequestService
  ) { }

  getCloud(url: string): any{
   return this._restService.getRequest(url);   
  }

  createPost(url: string, body: Post): Observable<Post> {
    return this._restService.postRequest(url, body) as Observable<Post>;
  }

}
