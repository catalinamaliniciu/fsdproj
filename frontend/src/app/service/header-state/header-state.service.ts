import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderStateService {
  emitter = new EventEmitter<boolean>();
  constructor() { }

  show() {
    this.emitter.emit(true);
  }

  hide() {
    this.emitter.emit(false);
  }
}
