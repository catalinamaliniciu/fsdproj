import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { RestRequestService } from './rest-request/rest-request.service';

export interface History {
  id: number;
  name: string;
  size: number;
  recognitionResult: string;
  link: string;

}

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  history: History[] = [];
  url: string;

  constructor(
    private _restService: RestRequestService
  ) { }

  getHistory(url: string): Observable<History[]>{
   return this._restService.getRequest(url) as Observable<History[]>;   
  }

  postHistory(url: string, body: any): Observable<History>{
    return this._restService.postRequest(url,body) as Observable<History>;
  }
}
