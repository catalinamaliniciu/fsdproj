import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { AuthGuard } from 'src/app/guards/auth-guard.guard';
import { catchError } from 'rxjs/operators';
import { TokenService } from '../token.service';
import { EMPTY, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestRequestService {

  constructor(private http: HttpClient,
    private _authGuard: AuthGuard,
    private _tokenService: TokenService) { }

  getRequest(url: string) {
    let options = this.addTokenToHeaders();
    return this.http.get(url, options).pipe(
      catchError(err => this.handleError(err))
    );
  }

  postRequest(url: string, body: any) {
    let options = this.addTokenToHeaders();
    return this.http.post(url, body, options).pipe(
      catchError(err => this.handleError(err))
    );
  }

  handleError(error: any) {
    if (error.error instanceof HttpErrorResponse && error.status === 401) {
      this._tokenService.removeToken() 
    }
      return throwError(error)
  }

  addTokenToHeaders() {
    let token = this._tokenService.getTokenOnly();
    if(token != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: this._tokenService.getTokenOnly()
        })
      };
      return httpOptions;
    }
  }
}
