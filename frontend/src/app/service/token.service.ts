import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtToken } from '../interfaces/token';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(
    private router: Router
  ) { }

  setToken(token: JwtToken) {
    localStorage.setItem('token', JSON.stringify(token))
  }

  getToken(): JwtToken {
    return JSON.parse(localStorage.getItem('token'))
  }

  getTokenOnly(): string {
    if(this.getToken() == null) {
      return null;
    } 
    return this.getToken().accessToken;
    
  }

  removeToken() {
    this.router.navigate(["login"]);
    this.setToken(null);
  }
}
