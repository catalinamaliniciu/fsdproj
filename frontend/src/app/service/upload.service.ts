import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { RestRequestService } from './rest-request/rest-request.service';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(
    private router: Router,
    private _restService: RestRequestService
  ) { }

  postImage(image: File, url: string){
    const formData: FormData = new FormData();
    formData.append('image', image, image.name);
    //const endpoint = 'http://localhost:3000/upload';
    return this._restService.postRequest(url, formData)
  }
}
