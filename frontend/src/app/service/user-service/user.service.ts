import { Injectable } from '@angular/core';
import { RestRequestService } from '../rest-request/rest-request.service';
import { tap } from 'rxjs/operators'
import { TokenService } from '../token.service';
import { JwtToken } from 'src/app/interfaces/token';
export interface User 
{
  email: string;
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userData: User;
  url: string;
  constructor(
    private _restService: RestRequestService,
    private _tokenService: TokenService
  ) { }

  getUserDetails() {
    this._restService.getRequest(this.url).subscribe((data: User) => {
      this.userData.email = data.email,
      this.userData.password = data.password
    })
  }

  setUserDetails() {
    let user = {
      email: this.userData.email,
      password: this.userData.password
    }
    this._restService.postRequest(this.url, user).subscribe();
  }

  login(user: User) {
   return this._restService.postRequest('http://localhost:3000/login', user).pipe(tap(
      (data: JwtToken) => {this._tokenService.setToken(data)},
    ))
  }
}
