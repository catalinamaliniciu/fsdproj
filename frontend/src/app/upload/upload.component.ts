import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HistoryService } from '../service/history.service';
//import { Component, ViewChild, ElementRef } from '@angular/core';
import { UploadService } from '../service/upload.service'


@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  constructor(
    private uploadService: UploadService,
    private historyService: HistoryService
  ) { }

  ngOnInit(): void {
  }

  url = "assets/cute-cat.png"
  onFileSelected(event) {
     var reader = new FileReader()
     reader.readAsDataURL(event.target.files[0])
     reader.onload = (event: any) => {
      this.url = event.target.result
     }
    
    const data = event.target.files[0]
    console.log(data)
    let label = this.uploadService.postImage(data, 'http://localhost:3000/upload')
      .subscribe(l => {
        console.log("Post image succesful ", l.toString())
        let history = {
          size : data.size,
          name : data.name,
          link : "test link",
          recognitionResult : l.toString()
        }
        this.historyService.postHistory('http://localhost:3000/history', history)
        .subscribe(d => {
          console.log("Post image succesful ", d.toString())
        }, error => {
          console.log(error);
        })
      }, error => {
        console.log(error);
      })

      
  }
}
