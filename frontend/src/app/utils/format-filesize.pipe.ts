import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatFilesize'
})
export class FormatFilesizePipe implements PipeTransform {

  transform(size: number, extension: string = 'MB'): string {
    return (size / (1024 * 1024)).toFixed(2) + extension;
  }

}
